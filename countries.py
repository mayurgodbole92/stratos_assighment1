import pandas as pd

df = pd.read_csv('data.csv')
#x = [x for x in input("Enter multiple countries: ").split(',')]
x=['Burundi','Germany','Curacao']
df_new = df.loc[(df['Country Name']).isin(x)]

starting_column='2011'
given_last_year='2020'

period_of_year= int(given_last_year)-int(starting_column)

for year in range(int(starting_column),int(given_last_year)):
    growth_percentage=((df_new[given_last_year].astype('float') - df_new[str(year)].astype('float'))*100/df_new[str(year)].astype('float'))
    per_year_growth=growth_percentage/10
    print( per_year_growth)

    current_year_population=  per_year_growth * df_new[given_last_year].astype('float')
    print("2021 the",current_year_population.astype(int))
