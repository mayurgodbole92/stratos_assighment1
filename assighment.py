import sys
import pandas as pd


    
class Menu:
    df = pd.read_csv('data.csv')
    # Menu Driven 
     
      
    # using the while loop to print menu list  
    while True:  
        print("MENU")  
        print("1. Data structure we used ")  
        print("2. Find the country/region with the highest and lowest population in a given year")  
        print("3. Find the country/region with the highest and lowest population growth percentage")
        print("4. Calculate the population by growth ratio")  
        print("5. Exit")  
        users_choice = int(input("\nEnter your Choice: "))  
      
   
        if users_choice == 1:  
            print( "Pandas Data Frame")  
         
      
        elif users_choice == 2:  
            getYear=input("Enter year")
            ''' second Quation'''
            if getYear in df.columns:
                print(df.nlargest(1,getYear)[['Country Name',getYear]])
                print(df.nsmallest(1,getYear)[['Country Name',getYear]])
            else:
                print("Wrong Year") 
            '''End'''

        elif users_choice == 3:
            
            start_column=input("Enter Starting year")
            ending_column=input("Enter Ending Year")
            if (start_column in df.columns) and ( ending_column in df.columns):
                growth_percentage=((df[ending_column].astype('float') - df[start_column].astype('float'))/df[start_column].astype('float'))*100
                
                df["growth_percentage"]=growth_percentage
                print(df.head())

                print(df.nlargest(1,'growth_percentage')[['Country Name','growth_percentage']])
                print(df.nsmallest(1,'growth_percentage')[['Country Name','growth_percentage']])
            else:
                print("Entered Series is not avaliable ")

        elif users_choice == 4:

             
            df_new = df.loc[(df['Country Name']).isin(x)]
            #print(df_new)
            starting_year='2011'
            given_last_year='2020'

           # period_of_year= int(given_last_year)-int(starting_column)
            nextYear=0
            year=[]

            
            x = [x for x in input("Enter multiple countries: ").split(',')]
            #x=['Burundi','Germany','Curacao']
            getNumberofyears= int(input("Enter Number of years"))
           
            
            for number in range(0,getNumberofyears):
                growth_percentage=((df_new[given_last_year].astype('float') - df_new[starting_year].astype('float'))/df_new[starting_year].astype('float')*100)
                per_year_growth=growth_percentage/10
                #print( per_year_growth)

                current_year_population=  (per_year_growth) * df_new[given_last_year].astype('float')
                next_year=current_year_population.astype('int')
                
                year.append(next_year)
            #df[int(given_last_year)*2]=year
            futurepopulationData = pd.DataFrame()
            
            futurepopulationData['CountryName']=x
            futurepopulationData[int(given_last_year)+2]=year
            print( futurepopulationData)  

      
        

      
      # exit the while loop
        elif users_choice == 5:  
            break  
          
        else:  
            print( "Please enter a valid Input from the list")  

if __name__ == "__main__":
    Menu().run()
